// Cookie banner
var dropCookie = true; // false disables the Cookie, allowing you to style the banner
var cookieDuration = 14; // Number of days before the cookie expires, and the banner reappears
var cookieName = 'complianceCookie'; // Name of our cookie
var cookieValue = 'on'; // Value of cookie
function createDiv(){
var bodytag = document.getElementsByTagName('body')[0];
var div = document.createElement('div');
div.setAttribute('id','cookie-law');
div.innerHTML = '<p>Denne hjemmeside benytter sig af cookies. Klik her for at se mere omkring vores brug af cookies <a href="cookie.html" target="_blank" rel="nofollow" title="Privacy &amp; Cookies Policy">privacy and cookies policy</a>. <a class="close-cookie-banner" href="javascript:void(0);" onclick="removeMe();"><span id="cookieclose">&times;</span></a></p>';

bodytag.insertBefore(div,bodytag.firstChild); // Adds the Cookie Law Banner just after the opening <body> tag
document.getElementsByTagName('body')[0].className+=' cookiebanner'; //Adds a class to the <body> tag when the banner is visible
createCookie(window.cookieName,window.cookieValue, window.cookieDuration); // Create the cookie
}
function createCookie(name,value,days) {
if (days) {
var date = new Date();
date.setTime(date.getTime()+(days*24*60*60*1000));
var expires = "; expires="+date.toGMTString();
}
else var expires = "";
if(window.dropCookie) {
document.cookie = name+"="+value+expires+"; path=/";
}
}
function checkCookie(name) {
var nameEQ = name + "=";
var ca = document.cookie.split(';');
for(var i=0;i < ca.length;i++) {
var c = ca[i];
while (c.charAt(0)==' ') c = c.substring(1,c.length);
if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
}
return null;
}
function eraseCookie(name) {
createCookie(name,"",-1);
}
window.onload = function(){
if(checkCookie(window.cookieName) != window.cookieValue){
createDiv();
}
}
function removeMe(){
var element = document.getElementById('cookie-law');
element.parentNode.removeChild(element);
}
// Cookie banner slut
// javascript til at ændre baggrunden på forsiden
function nordamerika(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/nordamerika.gif")';
    document.getElementById('nordamerika-text').style.display = 'block';
  }
function europa(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/europa.gif")';
    document.getElementById('europa-text').style.display = 'block';
  }
function asien(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/asien.gif")';
    document.getElementById('asien-text').style.display = 'block';
  }
function sydamerika(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/sydamerika.gif")';
    document.getElementById('sydamerika-text').style.display = 'block';
  }
function afrika(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/afrika.gif")';
    document.getElementById('afrika-text').style.display = 'block';
  }
function nordamerikaud(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/worldmap.gif")';
    document.getElementById('nordamerika-text').style.display = 'none';
  }
function europaud(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/worldmap.gif")';
    document.getElementById('europa-text').style.display = 'none';
  }
function asienud(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/worldmap.gif")';
    document.getElementById('asien-text').style.display = 'none';
  }
function afrikaud(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/worldmap.gif")';
    document.getElementById('afrika-text').style.display = 'none';
  }
function sydamerikaud(){
    document.getElementById('newbackground').style.backgroundImage = 'url("img/worldmap.gif")';
    document.getElementById('sydamerika-text').style.display = 'none';
  }
// Javascript til at ændre baggrunden på forsiden slut
// Javascript til læs mere/mindre knapper
function showMore(){
    document.getElementById('mobileHide').style.display = 'block';
    document.getElementById('mobileshow').style.display = 'none';
    document.getElementById('mobileshowless').style.display = 'block';
  }
function showLess(){
    document.getElementById('mobileHide').style.display = 'none';
    document.getElementById('mobileshow').style.display = 'block';
    document.getElementById('mobileshowless').style.display = 'none';
  }
$('#return-to-top').click(function() {      // When arrow is clicked
    $('body,html').animate({
        scrollTop : 0                       // Scroll to top of body
    }, 500);
});
// Javascript til læs mere/mindre knapper slut